var firebaseConfig = {
    apiKey: "AIzaSyA-bNtw3P8aDrCrisNHxsIMHDeqvGeQYzo",
    authDomain: "scoly-954f2.firebaseapp.com",
    projectId: "scoly-954f2",
    storageBucket: "scoly-954f2.appspot.com",
    messagingSenderId: "193976665302",
    appId: "1:193976665302:web:2506ddbace2d7fabc67577",
    measurementId: "G-Q6L9KPB6L9"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

const auth = firebase.auth();

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function verifyInput() {
    if (email.value == "") {
        alertBlock.innerHTML = `Please enter your email!`
        alertSection();
    } else if (!validateEmail(email.value)) {
        alertBlock.innerHTML = `Invalid Email! Make sure you enter a valid email.`
        alertSection();
    } else if (password.value == "") {
        alertBlock.innerHTML = `Please enter your password!`
        alertSection();
    } else {
        return true;
    }
};

const email = document.getElementById('email');
const password = document.getElementById('password');
const signinbtn = document.getElementById('signin-btn');
const alertBlock = document.getElementById('alertBlock');


signinbtn.addEventListener('click', e => {
    e.preventDefault();
    if (verifyInput()) {

        const femail = email.value;
        const fpassword = password.value;

        const promise = auth.signInWithEmailAndPassword(femail, fpassword);
        promise.catch(e => {
            if (e.code == 'auth/user-not-found') {
                alertBlock.innerHTML = `User not found!`
                alertSection();
            }
            console.log(e.code)
            if (e.code == 'auth/wrong-password') {
                alertBlock.innerHTML = `Incorrect password!`
                alertSection();
            }
        });
    }
    firebase.auth().onAuthStateChanged(firebaseUser => {
        if (firebaseUser) {
            window.location.href = "index.html"
        }
    })
})

function alertSection() {
    alertBlock.style.display = 'block';
    setTimeout(function () {
        alertBlock.style.display = 'none';
    }, 3000)
}