var firebaseConfig = {
    apiKey: "AIzaSyA-bNtw3P8aDrCrisNHxsIMHDeqvGeQYzo",
    authDomain: "scoly-954f2.firebaseapp.com",
    projectId: "scoly-954f2",
    storageBucket: "scoly-954f2.appspot.com",
    messagingSenderId: "193976665302",
    appId: "1:193976665302:web:2506ddbace2d7fabc67577",
    measurementId: "G-Q6L9KPB6L9"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

const auth = firebase.auth();
const db = firebase.firestore();

db.settings({
    timestampInSnapshots: true
});


let UserNames = [];

function snapshotToArray(snapshot) {
    var returnArr = [];

    snapshot.forEach(function (childSnapshot) {
        var item = childSnapshot.val();
        item.key = childSnapshot.key;

        returnArr.push(item);
    });

    UserNames = returnArr;
    return true
};

// Get Elements
const name = document.getElementById('name');
const email = document.getElementById('email');
const password = document.getElementById('password');
// const verifypassword = document.getElementById('verifypassword');
const signupbtn = document.getElementById('signup-btn');
const alertBlock = document.getElementById('alertBlock');




function signUpWithGoogle() {
    const googleProvider = new firebase.auth.GoogleAuthProvider();
    googleProvider.setCustomParameters({
        prompt: "select_account"
    });
    auth.signInWithRedirect(googleProvider);
}

// Validate Email
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function verifyInput() {
    if (name.value == "") {
        alertBlock.innerHTML = `Please enter your name!`
        alertSection();
    } else if (name.value.length < 3) {
        alertBlock.innerHTML = `Please enter a valid name!`
        alertSection();
    } else if (UserNames.includes(name.value)) {
        alertBlock.innerHTML = `This name already exists! Try signin in.`
        alertSection();
    } else if (email.value == "") {
        alertBlock.innerHTML = `Please enter your email!`
        alertSection();
    } else if (!validateEmail(email.value)) {
        alertBlock.innerHTML = `Invalid email! Please make sure you entered it correctly!`
        alertSection();
    } else if (password.value == "") {
        alertBlock.innerHTML = `Please Enter a password!`
        alertSection();
    } else if (password.value.length < 8) {
        alertBlock.innerHTML = `Password must be at least 8 characters!`
        alertSection();
    } else if (password.value.length > 16) {
        alertBlock.innerHTML = `Password must not exceed 16 characters!`
        alertSection();
    } else {
        return true;
    }
};

// Signing user up

signupbtn.addEventListener('click', e => {
    e.preventDefault();
    if (verifyInput()) {
        const vemail = email.value;
        const vpassword = password.value;


        const promise = auth.createUserWithEmailAndPassword(vemail, vpassword);
        promise.catch(e => {
            if (e.code == "auth/email-already-in-use")
                alertBlock.innerHTML = `This email already exists! Try signing in.`
            alertSection();
        })
    }
});

firebase.auth().onAuthStateChanged(firebaseUser => {
    if (firebaseUser) {
        db.collection('Users').add({
            name: name.value,
            email: email.value,
            password: password.value
        })

        firebase.database().ref().child("UserNames").push(name.value,
            function (error) {
                if (error) {
                    console.log("Data could not be saved." + error);

                } else {
                    firebase.database().ref().child("Users").child(firebaseUser.uid).child("Credentials").set([name.value, firebaseUser.email, password.value], function (error) {
                        if (error) {
                            console.log("Data could not be saved." + error);
                        } else {
                            window.location.href = "index.html"
                        }
                    })
                }
            }
        )
    }
});

function alertSection() {
    alertBlock.style.display = 'block';
    setTimeout(function () {
        alertBlock.style.display = 'none';
    }, 3000)
}