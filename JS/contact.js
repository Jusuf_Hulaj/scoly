var firebaseConfig = {
    apiKey: "AIzaSyA-bNtw3P8aDrCrisNHxsIMHDeqvGeQYzo",
    authDomain: "scoly-954f2.firebaseapp.com",
    projectId: "scoly-954f2",
    storageBucket: "scoly-954f2.appspot.com",
    messagingSenderId: "193976665302",
    appId: "1:193976665302:web:2506ddbace2d7fabc67577",
    measurementId: "G-Q6L9KPB6L9"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

var messagesRef = firebase.database().ref('Messages')
var firestore = firebase.firestore();
const db = firestore.collection("Contacted");

// document.querySelector('.contactbtn').addEventListener('click', contactValidation);

let regExp = /^[^ ]+@[^ ]+\.[a-z]{2,3}$/;


const name = document.getElementById('name')
const email = document.getElementById('email')
const message = document.getElementById('message')
const alerBlock = document.getElementById("alertBlock")
const contactBtn = document.getElementById('contactbtn')

function contactValidation() {
    if (name.value == '') {
        alertBlock.innerHTML = `Please enter your name!`
        alertSection()
    } else if (name.value.length <= 2) {
        alerBlock.innerHTML = `Please enter a valid name!`
        alertSection()
    } else if (email.value == '') {
        alerBlock.innerHTML = `Please enter an email!`
        alertSection()
    } else if (!email.value.match(regExp)) {
        alerBlock.innerHTML = `Please enter a valid email!`
        alertSection()
    } else if (message.value == '') {
        alerBlock.innerHTML = `Message cannot be empty!`
        alertSection()
    } else if (message.value.length < 50) {
        alerBlock.innerHTML = `Message should be longer than 50 characters!`
        alertSection()
    } else {
        return true
    }
}
// Submit Form
// function submitForm(e) {
//     e.preventDefault();

//     if(contactValidation()) {
//         var name = getValue('name');
//         var email = getValue('email');
//         var message = getValue('message');

//         saveMessage(name, email, message);
//         saveMessageToFirestore(name, email, message)
//         document.querySelector('.contact-form').reset();
//         alert('Thank your for your feedback!');
//     }
// }

// Function to get form values
function getValue(id) {
    return document.getElementById(id).value;
}

function saveMessage(name, email, message) {
    var newMessageRef = messagesRef.push();
    newMessageRef.set({
        name: name,
        email: email,
        message: message
    });
}

function saveMessageToFirestore(name, email, message) {

    db.doc().set({
        name: name,
        email: email,
        message: message
    }).then(function () {
        console.log('Data saved')
    }).catch(function (error) {
        console.log(error)
    });
}

function alertSection() {
    alertBlock.style.display = 'block';
    setTimeout(function () {
        alertBlock.style.display = 'none';
    }, 3000)
}

contactBtn.addEventListener('click', e => {
    e.preventDefault()

    if (contactValidation()) {
        var name = getValue('name');
        var email = getValue('email');
        var message = getValue('message');

        saveMessage(name, email, message);
        saveMessageToFirestore(name, email, message)
        document.querySelector('.contact-form').reset();
        alert('Thank your for your feedback!');
    }
})