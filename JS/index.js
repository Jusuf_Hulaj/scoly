var firebaseConfig = {
    apiKey: "AIzaSyA-bNtw3P8aDrCrisNHxsIMHDeqvGeQYzo",
    authDomain: "scoly-954f2.firebaseapp.com",
    projectId: "scoly-954f2",
    storageBucket: "scoly-954f2.appspot.com",
    messagingSenderId: "193976665302",
    appId: "1:193976665302:web:2506ddbace2d7fabc67577",
    measurementId: "G-Q6L9KPB6L9"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

const auth = firebase.auth();

firebase.auth().onAuthStateChanged(firebaseUser => {
    if (firebaseUser) {
        console.log(firebaseUser);
        document.getElementById('main-buttons').innerHTML = `<a href="dashboard.html" style="text-decoration: none;"><button class="dashboardbtn btn btn-primary mt-2" id="signup-btn">Dashboard</button></a>
                                                            <button class="logoutbtn btn btn-primary mt-2" id="logout-btn" onclick="logOut()">Log Out</button>`
    } else {
        console.log('Not Logged in!');
        document.getElementById('main-buttons').innerHTML = `<a href="signup.html" style="text-decoration: none;"><button class="registerBtn btn btn-primary btn-lg btn-block mt-2" id="signup-btn">Let the excitment begin!</button></a>`
    }
});

function logOut() {
    firebase.auth().signOut();
}